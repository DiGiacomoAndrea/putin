﻿(function ($) {
    var global = {};

    $.fn.putIn = function (options) {

        global = $.extend({
            _this: this,
            $tableWOrds: this.find("table[put-in-table]"),
            $inputWord: this.find("input[put-in-input]"),
            $wordsToSend: this.find("input[put-in-input-words]"),
            $btnPlus: this.find("button[put-in-btn-plus]"),
            separatorConcat: ","
        }, options);

        addEvents();

        return {

            addsToTable: function (items, separator) {
                var words = items;

                if (typeof items === "string") {
                    words = items.split(separator);
                }

                $.each(words, function (key, value) {
                    addToTable(value);
                });

                return global._this;
            }
        }
    };
    
    var addEvents = function () {
        global.$btnPlus.click(btnAddToTable);
        global._this.keypress(function (event) {
            if (event.keyCode === 13) {
                btnAddToTable();
            }
        });
    };

    var btnAddToTable = function() {
        var word = global.$inputWord.val();
        global.$inputWord.val("");
        addToTable(word);
    }

    var addToTable = function (word) {
        var $tbody = global.$tableWOrds.find("tbody");
        if (word.length > 0) {
            $tbody.append(createRow(word));
            if (global.$wordsToSend.length > 0)
                addWordResult(word);
        }
    };

    var createRow = function (word) {
        var row = $("<tr>");
        var cellWord = $("<td>").addClass("col-md-11").text(word);
        var cellBtn = $("<td>").addClass("text-center col-md-1");
        var icon = $("<i>").addClass("fa fa-trash");
        var btn = $("<button>").addClass("btn btn-danger btn-sm").attr("type", "button").data({word:word});

        btn.click(deleteItem);

        row.append(cellWord);
        row.append(cellBtn.append(btn.append(icon)));

        return row;
    };

    var deleteItem = function () {
        var word = $(this).data("word");
        $(this).closest("tr").remove();
        
        if (global.$wordsToSend.length > 0)
            deleteWordResult(word);
    };

    var addWordResult = function (word) {
        var words = [];
        var previousWords = global.$wordsToSend.val();

        if (previousWords !== "") 
            words = previousWords.split(global.separatorConcat);

        words.push(word);
        global.$wordsToSend.val(words.join(global.separatorConcat));
    };

    var deleteWordResult = function (word) {
        var words = global.$wordsToSend.val();
        var aWords = words.split(global.separatorConcat);

        aWords.splice(aWords.indexOf(word), 1);

        global.$wordsToSend.val(aWords.join(global.separatorConcat));
    }
}(jQuery));